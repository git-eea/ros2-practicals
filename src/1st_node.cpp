#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/twist.hpp"

class node : public rclcpp::Node {
public:
    node() : Node("node") {
        subscriber_ = create_subscription<geometry_msgs::msg::Twist>(
            "twist_topic", 10, std::bind(&node::twistCallback, this, std::placeholders::_1));
        publisher_ = create_publisher<std_msgs::msg::String>("string_topic", 10);
    }

private:
    void twistCallback(const geometry_msgs::msg::Twist::SharedPtr msg) {
        int lin_x = static_cast<int>(msg->linear.x);
        RCLCPP_INFO(this->get_logger(), "Received Twist message (linear.x: %d, angular.z: %f)", lin_x, msg->angular.z);

        // Create a String message and publish it
        auto string_msg = std::make_unique<std_msgs::msg::String>();
        string_msg->data = "Received Twist message";
        RCLCPP_INFO(this->get_logger(), "Publishing: '%s'(%d)", string_msg->data.c_str(), lin_x);
        publisher_->publish(std::move(string_msg));
    }

    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr subscriber_;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<node>());
    rclcpp::shutdown();
    return 0;
}


/*#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/twist.hpp"

class MyTwistStringNode : public rclcpp::Node {
public:
    MyTwistStringNode() : Node("my_twist_string_node") {
        publisher_ = create_publisher<geometry_msgs::msg::Twist>("twist_topic", 10);
        subscriber_ = create_subscription<std_msgs::msg::String>("string_topic", 10, std::bind(&MyTwistStringNode::stringCallback, this, std::placeholders::_1));
    }

private:
    void stringCallback(const std_msgs::msg::String::SharedPtr msg) {
        RCLCPP_INFO(this->get_logger(), "Received string: %s", msg->data.c_str());

        // Create a Twist message and publish it
        auto twist_msg = std::make_unique<geometry_msgs::msg::Twist>();
        twist_msg->linear.x = 1.0;
        twist_msg->angular.z = 0.5;
        publisher_->publish(std::move(twist_msg));
    }

    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
    rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscriber_;
};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<MyTwistStringNode>());
    rclcpp::shutdown();
    return 0;
}*/
