#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/twist.hpp"

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);
    auto node = std::make_shared<rclcpp::Node>("twist_publisher_node");
    auto publisher = node->create_publisher<geometry_msgs::msg::Twist>("twist_topic", 10);

    rclcpp::Rate rate(2);  // Publish at 1 Hz
    float t = 0;

    while (rclcpp::ok()) {
        t = t+1;
        auto twist_msg = std::make_unique<geometry_msgs::msg::Twist>();
        twist_msg->linear.x = t;
        twist_msg->angular.z = 1/t;
        publisher->publish(std::move(twist_msg));
        rate.sleep();
    }

    rclcpp::shutdown();
    return 0;
}
